import { TeacherService } from 'src/app/services/teacher/teacher.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { TeacherModel } from '../model/Teachers.model.class';
//import { Eps } from './../shared/models/eps';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {

  public form: FormGroup = new FormBuilder().group({
    numeroDocumento: new FormControl('', [Validators.required]),
    id: new FormControl('',),
    nombreTeacher: new FormControl('', [Validators.required]),
    apellidoTeacher: new FormControl('', [Validators.required]),
    edadTeacher: new FormControl('', [Validators.required])
  });

  isUpdate: boolean = false;
  isSave: boolean = false;
  teachersList = new Array<TeacherModel>();

  constructor(
    private fb: FormBuilder,
    private _teacherService: TeacherService
  ) {
  }

  ngOnInit(): void {

    this.GetAllTeachers();
  }

  registerTeacher(){

    //Obtener los valores del formulario
    const name = this.form.value.nombreTeacher;
    const lastName = this.form.value.apellidoTeacher;
    const age = this.form.value.edadTeacher;
    const numeroDocumento = this.form.value.numeroDocumento;

    if((name == "" && numeroDocumento == "" || lastName == "" || age == ""))
    {
      //Validacion opcional si la necesita
      Swal.fire(
        `Error!`,
        `Complete los campos primero.`,
        'error'
      );
    }

    this._teacherService.saveTeacher(this.prepareSave())
    .subscribe(data => {

      Swal.fire(
        '¡Éxito!',
        'Se ha registrado correctamente',
        'success'
      );

        this.form.reset();
        this.disableForm();
        this.GetAllTeachers();

        this.isUpdate = false;
        this.isSave = false;        

    },
      err =>         
     
      Swal.fire(
        `Se ha presentado un error al crear el profesor.`,
        'error'
      )
    );


}

  updateTeacher(){

  //Obtener los valores del formulario
  const name = this.form.value.nombreTeacher;
  const lastName = this.form.value.apellidoTeacher;
  const age = this.form.value.edadTeacher;
  const numeroDocumento = this.form.value.numeroDocumento;

  if((name == "" && numeroDocumento == "" || lastName == "" || age == ""))
  {
    //Validacion opcional si la necesita
    Swal.fire(
      `Error!`,
      `Complete los campos primero.`,
      'error'
    );
  }

  this._teacherService.updateTeacher(this.prepareUpdate())
  .subscribe(data => {

    Swal.fire(
      '¡Éxito!',
      'Se ha registrado correctamente',
      'success'
    );
    this.GetAllTeachers();

  },
    err =>         
   
    Swal.fire(
      `Se ha presentado un error al actualizar el profesor.`,
      'error'
    )
  );


}



  disableForm(){
    this.form.controls['nombreTeacher'].disable();
    this.form.controls['apellidoTeacher'].disable();
    this.form.controls['edadTeacher'].disable();
  }

  enableForm(){
    this.form.controls['nombreTeacher'].enable();
    this.form.controls['apellidoTeacher'].enable();
    this.form.controls['edadTeacher'].enable();
  }  

  limpiar(){

    this.form.controls['numeroDocumento'].enable();
    this.form.controls['numeroDocumento'].setValue("");
    this.form.controls['id'].setValue("");
    this.form.controls['nombreTeacher'].setValue("");
    this.form.controls['apellidoTeacher'].setValue("");
    this.form.controls['edadTeacher'].setValue("");
    this.isUpdate = false;
    this.isSave = false;    
    this.disableForm();

  }

  searchDocument(){

      //Activar inputs
      const numeroDocumento = this.form.value.numeroDocumento;

      if(numeroDocumento != null && numeroDocumento != "" && numeroDocumento > 0 ){

        this._teacherService.consultarTeacher(numeroDocumento).subscribe(res => {
             if(res != null){

              Swal.fire(
                `${numeroDocumento}`,
                `El número de documento se encuentra registrado.`,
                'success'
              );

              this.enableForm();

              //this.form.controls['numeroDocumento'].disable();
              this.form.controls['nombreTeacher'].setValue(res['name']);
              this.form.controls['id'].setValue(res['id']);
              this.form.controls['apellidoTeacher'].setValue(res['lastName']);
              this.form.controls['edadTeacher'].setValue(res['age']);

              this.isUpdate = true;
              this.isSave = false;


             }else{

              // no encontro usuario entonces se permite el registro

              this.form.controls['nombreTeacher'].setValue("");
              this.form.controls['id'].setValue("");
              this.form.controls['apellidoTeacher'].setValue("");
              this.form.controls['edadTeacher'].setValue("");              
              this.enableForm();

               Swal.fire(
                `${numeroDocumento}`,
                `No se ha encontrado No. Documento registrado`,
                'success'
              );               

               this.isUpdate = false;
               this.isSave = true;

             }
        });

      }
  }


  GetAllTeachers(){
    this._teacherService.consultarTeachers().subscribe(res => {
      this.teachersList = res['content'];
    });
  }


  prepareSave() {
    const formModel = this.form.value;
    const Teacher: TeacherModel = {
      id: 0,
      documentNumber: formModel.numeroDocumento as string,
      name: formModel.nombreTeacher as string,
      lastName: formModel.apellidoTeacher as string,
      age: formModel.edadTeacher as number,
      active : true
    };
    return Teacher;
  }  

  prepareUpdate() {
    const formModel = this.form.value;
    const Teacher: TeacherModel = {
      id: formModel.id as number,
      documentNumber: formModel.numeroDocumento as string,
      name: formModel.nombreTeacher as string,
      lastName: formModel.apellidoTeacher as string,
      age: formModel.edadTeacher as number,
      active : true
    };
    return Teacher;
  } 


}
