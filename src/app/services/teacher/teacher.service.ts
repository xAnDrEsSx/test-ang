import { TestCommonConstants } from '../../common/constants.class';
//import { BaseService } from '../base/base.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TeacherModel } from 'src/app/model/Teachers.model.class';

@Injectable({
  providedIn: 'root'
})

export class TeacherService {

  //private baseUrl: string = environment.baseUrl;
  constructor(private _http: HttpClient) {}

  saveTeacher(teacher: TeacherModel) {
    return this._http.post<TeacherModel>(TestCommonConstants.pathApi.teacher ,  teacher , { headers: this.generateBasicHeaders() });
    }  

  consultarTeachers() {
    let params = {
      page: 1,
      size: 10,
      enablePagination: false,
  };    

    return this._http.get<any>(TestCommonConstants.pathApi.teacher , { headers: this.generateBasicHeaders() , params: params });
  }


  consultarTeacher(documentNumber:String) {
    return this._http.get<any>(TestCommonConstants.pathApi.teacher + "/" + documentNumber , { headers: this.generateBasicHeaders() });
  }

  updateTeacher(teacher: TeacherModel) {
    return this._http.put<TeacherModel>(TestCommonConstants.pathApi.teacher ,  teacher , { headers: this.generateBasicHeaders() });
    }  

    protected generateBasicHeaders(): HttpHeaders {
      return new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }    


};