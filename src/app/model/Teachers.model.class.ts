export class TeacherModel{
    public id?: number;
    public documentNumber?: string;
    public name?: string;
    public lastName?: string;    
    public age?: number; 
    public active?: boolean;
}